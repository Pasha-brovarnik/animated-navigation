const menuBars = document.querySelector('#menu-bars');
const overlay = document.querySelector('#overlay');
const navItems = document.querySelectorAll('[id^="nav-"]');

// Control navigation animation
function navAnimation(direction1, direction2) {
	navItems.forEach((nav, i) => {
		removeAddClass(
			nav,
			`slide-${direction1}-${i + 1}`,
			`slide-${direction2}-${i + 1}`
		);
	});
}

// Remove and add class for selector
function removeAddClass(selector, removeClass, addClass) {
	selector.classList.remove(removeClass);
	selector.classList.add(addClass);
}

function toggleNav() {
	// Toggle: Menu Bars open/close
	menuBars.classList.toggle('change');
	// Toggle: Menu active
	overlay.classList.toggle('overlay-active');
	if (overlay.classList.contains('overlay-active')) {
		// Animate in - Overlay
		removeAddClass(overlay, 'overlay-slide-left', 'overlay-slide-right');
		// Animate in - Nav items
		navAnimation('out', 'in');
	} else {
		// Animate out - Overlay
		removeAddClass(overlay, 'overlay-slide-right', 'overlay-slide-left');
		// Animate out - Nav items
		navAnimation('in', 'out');
	}
}

// Event listeners
menuBars.addEventListener('click', toggleNav);
navItems.forEach((nav) => nav.addEventListener('click', toggleNav));
